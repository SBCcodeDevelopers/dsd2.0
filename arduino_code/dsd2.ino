
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <Vector.h>
#include "mux.h"
#include "sensor.h"

// Read Pin
const int pressurePin = A16;

const bool DEBUG = false;
const int BAUDRATE = 115200;

// Mux Setup
Mux* mux1 = new Mux(28, 26, 15, 19, 18);
Mux* mux2 = new Mux(27, 14, 8, 10, 9);
Mux* mux3 = new Mux(23, 17, 41, 45, 2);

// Sensors
Vector<Sensor> board1;
Sensor sensorStorageBuffer1[159];

void setup() {
  // Open serial communications and wait for port to open:
  // -----------------------------------------------------------------  
  Serial.begin(BAUDRATE); 
  
  // Board/Sensor Setup
  // -----------------------------------------------------------------
  board1.setStorage(sensorStorageBuffer1);
  int columnCount = 10;
  int rowLayout[11] = {26, 26, 26, 19, 14, 11, 9, 7, 5, 3, 1};
  for(int ix=0;ix<=columnCount;ix++) {
    for(int iy=0;iy<=rowLayout[ix];iy++) {
      Sensor s(ix, iy, mux1, mux2, mux3);                
      board1.push_back(s);           
    }
  }  
}

void sendData() {   
  Serial.println("B0");                 
  for (int i=0; i<board1.size(); i++) {            
    int buffer[3];    
    board1.at(i).serialize(buffer);  
    board1.at(i).debugPrint(); 
    Serial.println("S0");                 
    for (int j=0;j<3;j++) {        
      Serial.println(buffer[j]);    
      delay(5);      
    }
    Serial.println("S1");  
  }
  Serial.println("B1");               
}

void loop() {    

  if (Serial.available() > 2) {
    // Read the first byte
    byte cmdByte1 = Serial.read();
    byte cmdByte2 = Serial.read();
    
    if (cmdByte1== 198 && cmdByte2 == 48) {      
      Serial.flush();      
      sendData();       
    }
  }
  //delay(100);
}
