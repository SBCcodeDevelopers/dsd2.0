
#ifndef MUX_H
#define MUX_H

#include <Arduino.h>

class Mux {
  
  public:  	
    Mux(int pinE, int pin0, int pin1, int pin2, int pin3);
    void setChannel(int channel);
    int getChannel() { return mCurrentChannel; }    
    void setEnabled(bool isEnabled);
    void debugSerialPrint();

  private:    
  	int mPinE, mPin0, mPin1, mPin2, mPin3;    
  	int mCurrentChannel;
};

#endif