
#include "mux.h"

Mux::Mux(int pinE, int pin0, int pin1, int pin2, int pin3) {
	mPinE = pinE;
	mPin0 = pin0;
	mPin1 = pin1;
	mPin2 = pin2;
	mPin3 = pin3;

	// Configure
	pinMode(mPinE, OUTPUT); 
	pinMode(mPin0, OUTPUT); 
	pinMode(mPin1, OUTPUT); 
	pinMode(mPin2, OUTPUT); 
	pinMode(mPin3, OUTPUT);     
	  
	// Initialize to disable and low
  digitalWrite(mPinE, HIGH);  
	digitalWrite(mPin0, LOW);
	digitalWrite(mPin1, LOW);
	digitalWrite(mPin2, LOW);
	digitalWrite(mPin3, LOW);	
}

void Mux::debugSerialPrint() {
}

void Mux::setEnabled(bool isEnabled) {
	if (isEnabled) {
		digitalWrite(mPinE, LOW);  
	} else {
		digitalWrite(mPinE, HIGH);  
	}
}

void Mux::setChannel(int channel) {
	
  setEnabled(true);

	bool layout[16][4] = {
		{0, 0, 0, 0},		
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{1, 1, 0, 0},
		{0, 0, 1, 0},
		{1, 0, 1, 0},
		{0, 1, 1, 0},
		{1, 1, 1, 0},
		{0, 0, 0, 1},
		{1, 0, 0, 1},
		{0, 1, 0, 1},
		{1, 1, 0, 1},
		{0, 0, 1, 1},
		{1, 0, 1, 1},
		{0, 1, 1, 1},
		{1, 1, 1, 1}
	};

	digitalWrite(mPin0, layout[channel][0] ? HIGH: LOW);
	digitalWrite(mPin1, layout[channel][1] ? HIGH: LOW);
	digitalWrite(mPin2, layout[channel][2] ? HIGH: LOW);
	digitalWrite(mPin3, layout[channel][3] ? HIGH: LOW);
}
