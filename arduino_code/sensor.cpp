
#include "sensor.h"   
#include <Arduino.h>

Sensor::Sensor() {}

Sensor::Sensor(int x, int y, Mux *mux1, Mux *mux2, Mux *mux3):
	mX(0),
	mY(0) 
{    
	mX = x;
	mY = y;
	mMux1 = mux1;
	mMux2 = mux2;
	mMux3 = mux3;      
}

void Sensor::enable() 
{
	// Disable all muxes - setChannel will enable the given mux
	mMux1->setEnabled(false);
	mMux2->setEnabled(false);
	mMux3->setEnabled(false);

	// X - along width = mux3
	mMux3->setChannel(mX);

	// Y - along length = mux 1 & 2
	if (mY <= 10) {
		mMux2->setChannel(mY+5);
	} else {
		mMux1->setChannel(mY-11);
	}
}

int Sensor::read(int pin) 
{	
	int value = analogRead(pin);	
  return value;
}

void Sensor::serialize(int *buffer) 
{
  enable();

	buffer[0] = mX;
	buffer[1] = mY;
	buffer[2] = analogRead(A16);
}

void Sensor::debugPrint() 
{
//  Serial.println("----");
//  Serial.println(mX);
//  Serial.println(mY);
//  Serial.println("----");
}
