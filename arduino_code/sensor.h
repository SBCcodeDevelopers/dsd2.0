
#ifndef SENSOR_H
#define SENSOR_H

#include <Arduino.h>
#include "mux.h"

class Sensor {

	public:  	
    Sensor();
		Sensor(int x, int y, Mux *mux1, Mux *mux2, Mux *mux3);
		void enable();
		int read(int pin);
		void serialize(int *buffer);
		void debugPrint();

	private:    
		int mX;
		int mY;
		Mux* mMux1;
		Mux* mMux2;
		Mux* mMux3;

};

#endif
