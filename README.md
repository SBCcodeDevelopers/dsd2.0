# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* # DSD2 ESP32 Arduino Setup
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


## Links
* Adafruit Product Page = https://www.adafruit.com/product/3405
* Adafruit Wiring / Setup Guide = https://learn.adafruit.com/adafruit-huzzah32-esp32-feather

## Install Board In Arduino IDE
* Install the board via the Arduino board manager - https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md
* Install the CP2104 Driver - https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers
* Install ESPSoftwareSerial using arudino library manager

## DSD2 Slim3.5 Proto pinouts
* See: Cliff_Pinout/Slim3.5 sensor addresses.pdf

